function updateContentType(){
  const contentTypeInput = document.querySelector('#contentTypeHeader');
  const greetingsForm = document.querySelector('form');
  greetingsForm.enctype = contentTypeInput.checked ? 'text/plain' : 'application/x-www-form-urlencoded';
}